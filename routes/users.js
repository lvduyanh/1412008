var express = require('express');
var router = express.Router();
var csrf = require('csurf');
var passport = require('passport');

var account_controller = require('../controllers/accountController');

var csrfProtect = csrf();
router.use(csrfProtect);

router.get('/logout', isLogin, account_controller.log_out);

router.get('/change-pass', isLogin, account_controller.change_pass);

router.post('/change-pass', isLogin, account_controller.change_pass_post);

router.use('/', notLogin, function (req, res, next) {
    next();
});

router.get('/register', account_controller.register_get);

router.post('/register', account_controller.register_post, function (req, res, next) {
    if (req.session.oldUrl) {
        var oldUrl = req.session.oldUrl;
        req.session.oldUrl = null;
        res.redirect(oldUrl);
    } else {
        res.redirect('/');
    }
});

router.get('/account', account_controller.log_in_get);

router.post('/account', account_controller.log_in_post, function (req, res, next) {
    if (req.session.oldUrl) {
        var oldUrl = req.session.oldUrl;
        req.session.oldUrl = null;
        res.redirect(oldUrl);
    } else {
        res.redirect('/');
    }
});

router.get('/forgot', account_controller.resetPass);

router.post('/forgot', account_controller.resetPass_post);

module.exports = router;

function isLogin(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }
    res.redirect('/user/account');
}

function notLogin(req, res, next) {
    if (!req.isAuthenticated()) {
        return next();
    }
    res.redirect('/');
}