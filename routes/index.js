var express = require('express');
var router = express.Router();

var UserInfo = require('../models/userInfo');
var product_controller = require('../controllers/productController');
var dashboard_controller = require('../controllers/dashboardController');
var cart_controller = require('../controllers/cartController');
var account_controller = require('../controllers/accountController');
var tag_controller = require('../controllers/tagController');

/* GET home page. */
router.get('/' , product_controller.product_home);

router.get('/index' , product_controller.product_home);

router.get('/products', product_controller.product_list);

router.get('/viewpd/:id', product_controller.product_view);

router.get('/addtocart/:id', cart_controller.addItem);

router.get('/checkout', cart_controller.checkOut);

router.get('/coReduce1/:id', cart_controller.reduce1);

router.get('/coDel/:id', cart_controller.delitem);

router.get('/purchase', isLogin, cart_controller.purchase);

router.post('/purchase', isLogin, cart_controller.purchase_post);

router.get('/profile', isLogin, account_controller.profile);

router.get('/change-info', isLogin, account_controller.change_info);

router.post('/change-info', isLogin, account_controller.change_info_post);

router.get('/tag/:type', tag_controller.type);

router.get('/tag/:type/:producer', tag_controller.producer);

router.post('/search', product_controller.searchpd);

var adminRouter = express.Router({mergeParams: true});
router.use('/admin', adminRouter);

adminRouter.use('/', isLoginAdmin, function (req, res, next) {
    next();
});

adminRouter.get('/', dashboard_controller.dashboard_get);

adminRouter.get('/listpd', product_controller.am_getlistPd);

adminRouter.get('/product/delete/:id', product_controller.am_delPd);

adminRouter.get('/product/additem', product_controller.am_addPd);

adminRouter.post('/product/additem', product_controller.am_postAddPd);

adminRouter.get('/product/update/:id', product_controller.am_updPd);

adminRouter.post('/product/update/:id', product_controller.am_postUpdPd);

adminRouter.get('/listuser', account_controller.getUserList);

adminRouter.get('/user/delete/:id', account_controller.removeUser);

adminRouter.get('/user/update/:id', account_controller.updateUser);

adminRouter.post('/user/update/:id', account_controller.updateUserPost);

adminRouter.get('/addimage/:id', product_controller.add_image);

adminRouter.post('/addimage/:id', product_controller.add_image_post);

adminRouter.get('/statistic', product_controller.am_statistic);

module.exports = router;

function isLogin(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }
    req.session.oldUrl = req.originalUrl;
    res.redirect('/user/account');
}

function isLoginAdmin(req, res, next) {
    if (req.isAuthenticated()) {
        UserInfo.findOne({username: req.user.username}, function (err, ui) {
            if (ui != null && ui.type == 'admin') {
                return next();
            }
            else {
                res.redirect('/');
            }
        });
    }
    else {
        req.session.oldUrl = req.originalUrl;
        res.redirect('/user/account');
    }
}
