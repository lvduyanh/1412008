var passport = require('passport');
var Cart = require('../models/cart');
var Order = require('../models/order');
var UserInfo = require('../models/userInfo');
var User = require('../models/user');

exports.register_get = function (req, res, next) {
    var messages = req.flash('error');
    res.render('register', {csrfToken:  req.csrfToken(), mess: messages, hasErr: messages.length > 0});
};

exports.register_post = passport.authenticate('local.signup', {
        failureRedirect: '/user/register',
        failureFlash: true
});

exports.log_in_get = function (req, res, next) {
    var messages = req.flash('error');
    res.render('account', {csrfToken:  req.csrfToken(), mess: messages, hasErr: messages.length > 0});
};

exports.log_in_post = passport.authenticate('local.signin', {
    failureRedirect: '/user/account',
    failureFlash: true
});

exports.log_out = function (req, res, next) {
    req.logout();
    res.redirect('/');
};

exports.profile = function (req, res, next) {
    Order.find({user: req.user}, function(err, orders) {
        if (err) {
            return res.redirect('/');
        }
        var cart;
        orders.forEach(function(order) {
            cart = new Cart(order.cart);
            order.items = cart.generateArray();
        });
        UserInfo.findOne({username: req.user.username}, function (err, us) {
            console.log(us);
            if (err || us == null) {
                console.log('1111');
                var uinfo = new UserInfo();
                uinfo.username = req.user.username;
                uinfo.save(function (err, result) {
                    if (err) {
                        console.log('2222');
                        return res.redirect('/');
                    }
                    res.render('profile' , {un: req.user.username, email: req.user.email, user: us, active: 'Activated', orders: orders});
                });
            }
            else {
                console.log('3333');
                var at = 'Not Activated';
                if (us.status == 'ok') {
                    at = 'Activated';
                }
                res.render('profile', {
                    un: req.user.username,
                    email: req.user.email,
                    user: us,
                    active: at,
                    orders: orders
                });
            }
        });
    });
};

exports.change_info = function (req, res, next) {
    UserInfo.findOne({username: req.user.username}, function (err, us) {
        if (err || us == null) {
            return res.redirect('back');
        }
        res.render('changeinfo', {us: us});
    });
};

exports.change_info_post = function (req, res, next) {
    req.sanitize('name').trim();
    req.sanitize('name').escape();
    req.sanitize('address').trim();
    req.sanitize('address').escape();
    var nm = req.body.name;
    var ad = req.body.address;
    if (nm == '' || ad == '') {
        return res.redirect('/profile');
    }
    UserInfo.update({username: req.user.username}, {
        name: nm,
        address: ad
    }, function (err) {
        if (err) {
            console.log(err);
        }
        return res.redirect('/profile');
    })
};

exports.change_pass = function (req, res, next) {
    res.render('changepass', {csrfToken:  req.csrfToken()});
};

exports.change_pass_post = function (req, res, next) {
    //console.log(req.body);
    var pass = req.body.curpass;
    var newpass = req.body.pass;
    var repass = req.body.repass;
    if (newpass != repass) {
        res.render('changepass', {csrfToken:  req.csrfToken(), mess: "New pass and re-enter new pass don't match."});
        return;
    }
    req.checkBody('pass', 'Invalid password').notEmpty().isLength({min: 4});
    var errors = req.validationErrors();
    if (errors) {
        res.render('changepass', {csrfToken:  req.csrfToken(), mess: "Invalid password."});
        return;
    }
    User.findOne({username: req.user.username}, function (err, us) {
        if (!us.validPassword(pass)) {
            res.render('changepass', {csrfToken:  req.csrfToken(), mess: 'Wrong password.'});
            return;
        }
        us.password = us.encyptPassword(newpass);
        User.update({username: us.username}, {password: us.password}, function (err, rs) {
            if (err) {
                res.render('changepass', {csrfToken:  req.csrfToken()});
            }
            else
                res.render('changepass', {csrfToken:  req.csrfToken(), mess: 'Success.'});
        });
    });
};

exports.resetPass = function (req, res, next) {
    res.render('resetpass', {csrfToken:  req.csrfToken()});
};

exports.resetPass_post = function (req, res, next) {
    var em = req.body.email;
    var uname = req.body.username;
    User.findOne({username: uname, email: em}, function (err, us) {
        if (err || us == null) {
            res.render('resetpass', {csrfToken:  req.csrfToken()});
            return;
        }
        var newpass = generateNewPass();
        //console.log(newpass);
        us.password = us.encyptPassword(newpass);
        User.update({username: us.username}, {password: us.password}, function (err, rs) {
            transporter.sendMail({
                from: 'noreplydawck2017@gmail.com',
                to: em,
                subject: 'Reset pass ' + us.username,
                template: 'mailreset',
                context: {
                    layout: false,
                    un: us.username,
                    pw: newpass
                }
            }, function (err, rs) {
                if (err) {
                    console.log(err);
                    res.render('resetpass', {csrfToken:  req.csrfToken(), mess: 'Error: contact admin lvduyanh@gmail.com'});
                    return;
                }
                res.render('resetpass', {csrfToken:  req.csrfToken(), mess: 'Email has been sent.'});
            });
        });
    });
};

exports.getUserList = function (req, res, next) {
    User.find({}, function(err, users) {
        if (err) {
            console.log(err);
            return res.redirect('/admin');
        }
        res.render('admin/users', {
            layout: 'admin/admin_layout',
            title: "Danh sách user",
            users: users
        });
    });
};

exports.removeUser = function (req, res, next) {
    User.findOne({"_id": req.params.id}, function (err, us) {
        if (err || us == null) {
            return res.redirect('back');
        }
        if (us.username != req.user.username) {
            User.remove({"_id": req.params.id}, function (err) {
                if (err) {
                    console.log(err);
                }
                UserInfo.remove({username: us.username}, function (err2) {
                    res.redirect('back');
                });

            });
        }
        else {
            res.redirect('back');
        }
    });
};

exports.updateUser = function (req, res, next) {
    var idu = req.params.id;
    User.findOne({"_id": idu}, function (err, us) {
        if (err || us == null) {
            console.log(err);
            res.redirect('back');
        }
        if (us.username == req.user.username) {
            res.redirect('back');
        }
        UserInfo.findOne({username: us.username}, function(err2, ui) {
            if (err2) {
                console.log(err2);
                res.redirect('back');
            }
            res.render('admin/update_user', {layout: 'admin/admin_layout', us: us, ui: ui});
        });
    });
};

exports.updateUserPost = function (req, res, next) {
    var uname = req.body.username;
    var em = req.body.email;
    var nm = req.body.name;
    var adr = req.body.address;
    var type = req.body.type;
    var stt = req.body.status;
    if (uname == req.user.username) {
        res.redirect('/admin/listuse');
    }
    var us = new User();
    us.username = uname;
    us.email = em;
    var ui = new UserInfo();
    ui.name = nm;
    ui.address = adr;
    ui.type = type;
    ui.status = stt;
    User.update({username: uname}, {email: em}, function(err) {
        if (err) {
            console.log(err);
            res.render('admin/update_user', {
                layout: 'admin/admin_layout',
                us: us, ui: ui,
                mess: '<div class="alert alert-warning">Thất bại!</div>'
            });
        }
        UserInfo.update({username: uname}, {
            name: nm,
            address: adr,
            type: type,
            status: stt
        }, function(err2) {
            if (err2) {
                console.log(err2);
                res.render('admin/update_user', {
                layout: 'admin/admin_layout',
                us: us, ui: ui,
                mess: '<div class="alert alert-warning">Thất bại!</div>'
            });
            }
            res.redirect('/admin/listuser');
        });
    });
    
};

function generateNewPass() {
	var text = "";
	var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	for( var i=0; i < 8; i++ )
		text += possible.charAt(Math.floor(Math.random() * possible.length));

	return text;

}