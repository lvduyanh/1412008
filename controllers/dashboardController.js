var Product = require('../models/product');

exports.dashboard_get = function (req, res, next) {
    Product.find({}, function (err, ListProduct) {
        if (err){
            console.log(err);
            return;
        }
        var a = 0, b = 0, c = 0, d = 0;
        for(var i=0; i<ListProduct.length; i++) {
            if(ListProduct[i].category=="mobile") {
                a++;
            }
            else if(ListProduct[i].category=="laptop") {
                b++;
            }
            else if(ListProduct[i].category=="camera") {
                c++;
            }
            else {
                d++;
            }
        }
        res.render('admin/dashboard',{
            layout: 'admin/admin_layout',
            title:"Welcome to Admin page",
            total: ListProduct.length,
            catA: a,
            catB: b,
            catC: c,
            catD: d
        });
    });
}
