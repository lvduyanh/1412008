var Product = require('../models/product');
var ProductInfo = require('../models/productinfo');
var Images = require('../models/pdimage');

exports.product_home = function(req, res, next) {
    Product.find({}, function (err, products) {
        if (err){
            console.log(err);
            return;
        }
        if (products == null || products.length == 0) {
            res.render('index', {products: '<p>No product to show.</p>'});
        }
        else {
            var features = [];
            var str = '';
            var av = [];
            for (var i = 0; i < products.length; i++) {
                if (i < 9) {
                    if (i != 0 && i % 3 == 0) {
                        str += '</div>\n';
                    }
                    if (i % 3 == 0) {
                        str += '<div class="row">\n'
                    }
                    str += '<div class="col-md-4 product simpleCart_shelfItem text-center">\n';
                    str += '<a href="/viewpd/'+ products[i].id + '"><img src="/images/' + products[i].avatar + '" alt="" /></a>\n';
                    str += '<div class="mask">\n';
                    str += '<a href="/viewpd/' + products[i].id +'">Quick View</a>\n';
                    str += '</div>\n';
                    str += '<a class="product_name" href="/viewpd/' +products[i].id +'">' + products[i].name + '</a>\n';
                    str += '<p><a class="item_add" href="#"><i></i> <span class="item_price">' + products[i].price + '</span></a></p>\n';
                    str += '</div>\n';
                }
                if (i < 5) {
                    features.push(products[i]);
                }
                av.push(products[i].name);
            }
            str += '</div>\n';
            res.locals.avai = JSON.stringify(av);
            res.render('index', {products: str, features: features});
        }
    });
};

exports.product_list = function(req, res, next) {
    Product.find({}, function (err, products) {
        if (err) {
            console.log(err);
            return;
        }
        res.render('products', {items: products});
    });
};

exports.product_view = function(req, res, next) {
    Product.findById(req.params.id, function (err, pd) {
        if (err || pd == null){
            console.log(err);
            res.redirect('/products');
            return;
        }
        ProductInfo.findOne({idpd: req.params.id}, function (err, pi) {
            if (pi == null) {
                pi = new ProductInfo();
                pi.idpd = req.params.id;
                pi.save();
            }
            var v = pi.view + 1;
            pi.update({view: v}, function (err2) {
                Images.findOne({idpd: req.params.id}, function (err3, imgs) {
                    Product.find({category: pd.category}, function (err4, rlpd) {
                        var rs = [];
                        for (var i = 0; i < rlpd.length; i++) {
                            if (rlpd[i].name != pd.name) {
                                rs.push(rlpd[i]);
                                if (rs.length == 3) {
                                    break;
                                }
                            }
                        }
                        res.render('viewpd', {
                            id: req.params.id, name: pd.name, img: pd.avatar,
                            detail: pd.info, price: pd.price, quantity: pd.quantity,
                            subimg: imgs, producer: pi.nsx.toUpperCase(), view: pi.view,
                            relate: rs
                        });
                    });
                });
            });
        });
    });
};

exports.am_getlistPd = function(req, res, next) {
    Product.find({}, function (err, products) {
        if (err) {
            console.log(err);
            return;
        }
        res.render('admin/dssp', {
            layout: 'admin/admin_layout',
            title: "Danh sách sản phẩm",
            products: products
        });
    });
};

exports.am_delPd = function(req, res, next) {
    var pdid = req.params.id;
    Product.remove({"_id": pdid}, function (err) {
        if (err) {
            console.log(err);
        }
        ProductInfo.remove({idpd: pdid}, function (err2) {
            Images.remove({idpd: pdid}, function (err2) {
                res.redirect('/admin/listpd');
            });
        });
    });
};

exports.am_addPd = function(req, res, next) {
    res.render('admin/add',{
        layout: 'admin/admin_layout',
        title: 'Thêm sản phẩm'
    });
};

exports.am_postAddPd = function(req, res, next) {
    if(checkEmptyForm(req.body.name, req.body.category, req.body.price, req.body.image, req.body.detail, req.body.quantity)) {
        res.render('admin/add', {
            layout: 'admin/admin_layout',
            title: 'Thêm sản phẩm',
            message: '<div class="alert alert-warning">Vui lòng nhập đầy đủ thông tin!</div>'
        });
        return;
    }
    var cat = getCat(req.body.category);
    var product_instance = new Product({
        name: req.body.name,
        category: cat,
        price: req.body.price,
        quantity: req.body.quantity,
        info: req.body.detail,
        avatar: req.body.image
    });
    product_instance.save(function (err) {
        if (err) {
            console.log(err);
            res.render('admin/add', {
                layout: 'admin/admin_layout',
                title: 'Thêm sản phẩm',
                message: '<div class="alert alert-danger">Thêm thất bại</div>'
            });
            return;
        }
        var pi = new ProductInfo();
        pi.idpd = product_instance.id;
        pi.nsx = req.body.nsx.toLowerCase();
        pi.save(function (err) {
            res.render('admin/add',{
                layout: 'admin/admin_layout',
                title: 'Thêm sản phẩm',
                message: '<div class="alert alert-success">Thêm thành công!</div>'
            });
        });
    });
};

exports.am_updPd = function(req, res, next) {
    Product.findById(req.params.id, function (err, CurrentItem) {
        if (err || CurrentItem == null){
            console.log(err);
            return res.redirect('/admin/listpd');
        }
        ProductInfo.findOne({idpd: req.params.id}, function (err2, pi) {
            //console.log(pi);
            if (pi == null) {
                pi = new ProductInfo();
                pi.idpd = req.params.id;
                pi.save(function (err) {
                    res.render('admin/update',{
                        layout: 'admin/admin_layout',
                        title: 'Cập nhật sản phẩm',
                        pd: CurrentItem,
                        pi: pi
                    });
                });
            }
            else {
                res.render('admin/update',{
                    layout: 'admin/admin_layout',
                    title: 'Cập nhật sản phẩm',
                    pd: CurrentItem,
                    pi: pi
                });
            }
        });
    });
};

exports.am_postUpdPd = function(req, res, next) {
    if(checkEmptyForm(req.body.name, req.body.category, req.body.price, req.body.image, req.body.detail, req.body.quantity)) {
        return res.redirect('back');
    }
    console.log(req.body);
    var cat = getCat(req.body.category);
    Product.update({"_id": req.params.id}, {name: req.body.name, category: cat, price: req.body.price,
            quantity: req.body.quantity, info: req.body.detail, avatar: req.body.image},
        function (err) {
            if (err) {
                console.log(err);
                return res.redirect('/admin/listpd');
            }
            ProductInfo.update({idpd: req.params.id}, {nsx: req.body.nsx.toLowerCase()}, function (err2) {
                if (err2)
                    console.log(err2);
                res.redirect('/admin/listpd');
            });
        });
};

exports.add_image = function (req, res, next) {
    var pdid = req.params.id;
    Product.findOne({_id: pdid}, function (err, pd) {
        if (pd == null) {
            console.log('pd = null');
            res.redirect('back');
        }
        else {
            Images.findOne({idpd: pdid}, function (err2, img) {
                if (img == null) {
                    img = new Images();
                    img.idpd = pdid;
                    img.save(function (err) {
                        res.render('admin/product_image', {
                            layout: 'admin/admin_layout',
                            title: 'Add product images',
                            img0: pd.avatar,
                            img1: '',
                            img2: '',
                            id: pdid
                        });
                    });
                }
                else {
                    res.render('admin/product_image', {
                        layout: 'admin/admin_layout',
                        title: 'Add product images',
                        img0: pd.avatar,
                        img1: img.img1,
                        img2: img.img2,
                        id: pdid
                    });
                }
            });
        }
    });
};

exports.add_image_post = function (req, res, next) {
    var pdid = req.params.id;
    Product.findOne({_id: pdid}, function (err, pd) {
        if (pd == null) {
            res.redirect('back');
        }
        else {
            var i1 = req.body.img1, i2 = req.body.img2;
            Images.findOne({idpd: pdid}, function (err2, img) {
                if (img == null) {
                    return res.redirect('back');
                }
                else {
                    Images.update({idpd: pdid}, {
                        img1: i1,
                        img2: i2,
                        id: pdid
                    }, function (err) {
                        res.redirect('/admin/product/update/' + pdid);
                    });
                }
            });
        }
    });
};

exports.am_statistic = function (req, res, next) {
    Product.find({}, function (err, pds) {
        var rs = [];
        var l = pds.length;
        //var len2 = pds.length;
        pds.forEach(function (pd, idx) {
            ProductInfo.findOne({idpd: pd.id}, function (err, pi) {
                if (pi != null && pd!= null) {
                    var tmp = {
                        id: pd.id,
                        name: pd.name,
                        view: pi.view,
                        sold: pi.sold
                    };
                    rs.push(tmp);
                }
                // else {
                // 	len2 -= 1;
                // }
                if (idx >= (l - 1) /*&& rs.length >= len2*/){
                    rs.sort(function (a, b) {
                        return a.view - b.view;
                    });
                    res.render('admin/statistic', {
                        layout: 'admin/admin_layout',
                        title: 'Statistic',
                        pds: rs
                    });
                }
            });
        });
    });
}

exports.searchpd = function (req, res, next) {
    var str = req.body.search;
    Product.findOne({name: str}, function (err, pd) {
        if (err || pd == null){
            return res.redirect('back');
        }
        res.redirect('/viewpd/' + pd.id);
    });
}

function checkEmptyForm(name, category, price, image, detail, quantity)
{
    if(name==null||name.length==0||category.length==0||price==null||image==null||image.length==0||
        detail==null||detail.length==0||quantity==null||quantity.length==0) {
        return true;
    }
    else {
        return false;
    }
}

function getCat(str) {
    var cat = '';
    if(str == 'Mo') {
        cat = 'mobile';
    }
    else if(str == 'Ca') {
        cat = 'camera';
    }
    else if(str == 'La') {
        cat = 'laptop';
    }
    else {
        cat = 'headphone';
    }
    return cat;
}