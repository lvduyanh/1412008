var Product = require('../models/product');
var ProductInfo = require('../models/productinfo');

exports.type = function (req, res, next) {
    var tp = req.params.type;
    Product.find({category: tp}, function (err, products) {
        res.render('products', {items: products});
    });
}

exports.producer = function (req, res, next) {
    var tp = req.params.type;
    var pdc = req.params.producer;
    if (tp == 'all') {
        ProductInfo.find({nsx: pdc}, function (err, pis) {
            var rs = [];
            var l = pis.length;
            pis.forEach(function(pi, idx) {
                Product.findOne({_id: pi.idpd}, function (err, pd) {
                    if (pd != null) {
                       rs.push(pd);
                    }
                    if (idx >= (l - 1)) {
                        res.render('products', {items: rs});
                    }
                });
            });
        });
    }
    else {
        ProductInfo.find({nsx: pdc}, function (err, pis) {
            var rs = [];
            var l = pis.length;
            pis.forEach(function(pi, idx) {
                Product.findOne({_id: pi.idpd, category: tp}, function (err, pd) {
                    if (pd != null) {
                        rs.push(pd);
                    }
                    if (idx >= (l - 1)) {
                        res.render('products', {items: rs});
                    }
                });
            });
        });
    }
}