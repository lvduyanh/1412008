var Cart = require('../models/cart');
var Product = require('../models/product');
var Order = require('../models/order');

exports.addItem = function (req, res, next) {
    var pdId = req.params.id;
    var cart = new Cart(req.session.cart ? req.session.cart : {});
    Product.findById(pdId, function (err, pd) {
       if (err) {
           res.redirect('/products');
       }
       cart.add(pd, pd.id);
       req.session.cart = cart;
       console.log(req.session.cart);
        res.redirect('back');
    });
};

exports.checkOut = function (req, res, next) {
    var scMess = req.flash('success')[0];
    if (!req.session.cart) {
        return res.render('checkout', {products: null, mess: scMess});
    }
    var cart = new Cart(req.session.cart);
    return res.render('checkout', {products: cart.generateArray(), total: cart.totalPrice});
};

exports.reduce1 = function (req, res, next) {
    var pdId = req.params.id;
    var cart = new Cart(req.session.cart ? req.session.cart : {});
    cart.reduceByOne(pdId);
    req.session.cart = cart;
    res.redirect('/checkout');
};

exports.delitem = function (req, res, next) {
    var pdId = req.params.id;
    var cart = new Cart(req.session.cart ? req.session.cart : {});
    cart.removeItem(pdId);
    req.session.cart = cart;
    res.redirect('/checkout');
};

exports.purchase = function (req, res, next) {
    if (!req.session.cart) {
        return res.render('checkout', {products: null});
    }
    var cart = new Cart(req.session.cart);
    var errMess = req.flash('error')[0];
    return res.render('purchase', {total: cart.totalPrice, errMess: errMess});
};

exports.purchase_post = function (req, res, next) {
    if (!req.session.cart) {
        return res.render('checkout', {products: null});
    }
    var cart = new Cart(req.session.cart);
    var stripe = require("stripe")(
        "sk_test_CltM4cKvXn4OP4Lewv7saY0Y"
    );
    stripe.charges.create({
        amount: cart.totalPrice,
        currency: "vnd",
        source: req.body.stripeToken,
        description: "DuyAnh-sama"
    }, function(err, charge) {
        if (err) {
            req.flash('error', err.message);
            return res.redirect('/purchase');
        }
        var order = new Order({
            user: req.user,
            cart: cart,
            address: req.body.address,
            name: req.body.name,
            paymentId: charge.id,
            purchase: true
        });
        order.save(function(err, result) {
            if (err) {}
            req.flash('success', 'Bought successful!');
            req.session.cart = null;
            res.redirect('/checkout');
        });
    });
};
