var Product = require('./models/product');
var mongoose = require('mongoose');
var mongoDB = 'mongodb://127.0.0.1/QLSP';
mongoose.connect(mongoDB);

var arr_product = [
    new Product({
        name: 'iPhone 7 128GB RED',
        category: 'mobile',
        price: 19999000,
        quantity: 10,
        info: 'iPhone 7 đỏ 128 GB (iPhone 7 RED) ra mắt một cách âm thầm đầy bất ngờ đã khiến người hâm mộ sửng sốt. Một iPhone 7 mới không nâng cấp về cấu hình nhưng mang bộ áo đỏ rực rỡ và đẹp một cách tinh tế khiến bất kỳ ai cũng mơ ước muốn được sở hữu.',
        avatar: 'p11.jpg'
    }),
    new Product({
        name: 'iPhone 6s Plus 64GB',
        category: 'mobile',
        price: 14999000,
        quantity: 15,
        info: 'iPhone 6 Plus là một phiên bản iPhone màn hình lớn nhất và cao cấp nhất của Apple từ trước đến nay, nhưng nó không chỉ đơn thuần là lớn hơn về kích thước mà còn tốt hơn về mọi mặt. ',
        avatar: 'p21.jpg'
    }),
    new Product({
        name: 'Samsung Galaxy S7 Edge',
        category: 'mobile',
        price: 15490000,
        quantity: 20,
        info: 'Samsung Galaxy S7 edge là sự tiếp nối của phiên bản S6 edge mà Samsung đã cho ra mắt hồi đầu năm 2015. Với sự nâng cấp mạnh mẽ về cấu hình cùng nhiều tính năng mới đi kèm, chiếc điện thoại này chắc chắn sẽ làm hài lòng những tín đồ trung thành của thương hiệu Hàn Quốc này!',
        avatar: 'p31.jpg'
    }),
    new Product({
        name: 'Samsung Galaxy S8 Plus',
        category: 'mobile',
        price: 20490000,
        quantity: 16,
        info: 'Galaxy S8 Plus ra mắt đã làm hài lòng giới công nghệ lẫn người tiêu dùng hâm mộ hãng điện thoại danh tiếng đến từ Hàn Quốc – Samsung.',
        avatar: 'p41.jpg'
    }),
    new Product({
        name: 'Macbook Pro 15 Touch Bar 256GB [2017]',
        category: 'laptop',
        price: 56999000,
        quantity: 9,
        info: 'MacBook Pro 15 Touch Bar ra mắt với nhiều đột phá cả về thiết kế lẫn cấu hình. Nhanh hơn, mạnh mẽ hơn, mỏng hơn và nhẹ hơn.',
        avatar: 'p51.jpg'
    }),
    new Product({
        name: 'Dell Ins N7566A',
        category: 'laptop',
        price: 25990000,
        quantity: 12,
        info: 'Dell Ins N7566A có thiết mạnh mẽ, cá tính cùng cấu hình vô cùng mạnh mẽ với bộ vi xử lý Core i7 Skylake thế hệ mới nhất của Intel kết hợp cùng bộ nhớ RAM 8 GB và card đồ họa rời NVIDIA GeForce GTX 960M 4GB, hứa hẹn sẽ mang đến cho người dùng những trải nghiệm khác biệt và mượt mà nhất.',
        avatar: 'p61.jpg'
    }),
    new Product({
        name: 'MSI GL62 7RD - 675XVN',
        category: 'laptop',
        price: 22990000,
        quantity: 14,
        info: 'Với MSI GL62-7RD, đây được đánh giá là chiếc laptop gaming toàn diện nhất hiện nay nhờ độ hoàn thiện tốt trong thiết kế, hiệu năng mạnh mẽ và các công nghệ của nó đều hỗ trợ người dùng rất tốt.',
        avatar: 'p71.jpg'
    }),
    new Product({
        name: 'Asus UX410UA-GV064',
        category: 'laptop',
        price: 15990000,
        quantity: 20,
        info: 'ASUS UX410UA-GV064 là chiếc laptop 14 inch đầu tiên của ASUS được thiết kế trên thân máy 13 inch, nhờ việc sử dụng viền màn hình siêu mỏng 6 mm.',
        avatar: 'p81.jpg'
    }),
    new Product({
        name: 'Beats Solo 3 White MNEP2ZA/A',
        category: 'headphone',
        price: 6199000,
        quantity: 20,
        info: 'Tai nghe choàng đầu Beats Solo 3 White có dải sub bass tuy nhiều và tràn về lượng nhưng lại đánh khá sâu, rất dễ để làm hài lòng người nghe phổ thông hoặc các basshead (những người thích âm bass).',
        avatar: 'p91.jpg'
    }),
    new Product({
        name: 'Sony XB450AP',
        category: 'headphone',
        price: 1490000,
        quantity: 30,
        info: 'Màng loa Dynamic rộng 30mm, tái tạo âm Bass sâu, chắc đầy mạnh mẽ. Đệm tai êm, giúp thoải mái nghe nhạc trong thời gian dài. Dải âm tần: 5 - 22,000Hz. Độ nhạy 102dB/mW',
        avatar: 'p101.jpg'
    }),
    new Product({
        name: 'Bluetooth Ibomb SKA G50',
        category: 'headphone',
        price: 799000,
        quantity: 31,
        info: 'Vì là tai nghe không dây, nên iBomb SKA có tính cơ động rất cao, người dùng có thể gập hoặc mở ear-cups và bỏ vào balo, túi xách một cách dễ dàng.',
        avatar: 'p111.jpg'
    }),
    new Product({
        name: 'Remax Sport RM-S2',
        category: 'headphone',
        price: 599000,
        quantity: 40,
        info: 'Bluetooth 4.1, dung lượng PIN lớn sử dụng thoải mái 24h. Kiểu dáng thời trang, thể thao, có nút tăng giảm âm lượng trên dây.',
        avatar: 'p121.jpg'
    }),
    new Product({
        name: 'FUJIFILM X-E2S',
        category: 'camera',
        price: 14990000,
        quantity: 12,
        info: 'Máy ảnh Fujifilm X-E2S tích hợp những công nghệ độc quyền của hãng và thêm vào đó là phần cứng chất lượng cao, sẽ mang đến bạn những bức ảnh cực nét và nghệ thuật.',
        avatar: 'p131.jpg'
    }),
    new Product({
        name: 'CANON POWERSHOT G7X',
        category: 'camera',
        price: 11700000,
        quantity: 15,
        info: 'Máy ảnh Canon PowerShot G7X là dòng máy ảnh compact cho chất lượng hình ảnh cực kỳ nét.',
        avatar: 'p141.jpg'
    }),
    new Product({
        name: 'SONY DSC-WX220/NCE32',
        category: 'camera',
        price: 3790000,
        quantity: 22,
        info: 'Máy ảnh Sony DSC-WX220 hội đủ các yếu tố kiểu dáng đẹp, chụp hình sắc nét và giá thành phải chăng.',
        avatar: 'p151.jpg'
    }),
    new Product({
        name: 'SONY HDR CX405/BCE35',
        category: 'camera',
        price: 6490000,
        quantity: 18,
        info: 'Máy quay phim Sony HDR-CX405 thuộc dòng máy quay gia đình, lưu lại những khoảnh khắc thú vị trong cuộc sống với hình ảnh sắc nét, mịn màng. Máy quay phim Sony HDR-CX405 có thiết kế dạng cầm tay tiện lợi, giúp bạn dễ dàng cầm mà không lo rung lắc khi quay.',
        avatar: 'p161.jpg'
    })
];

var done = 0;
for (var i = 0; i < arr_product.length; i++) {
    arr_product[i].save(function (err, result) {
        done++;
        if (done == arr_product.length) {
            exit();
        }
    });
}

function exit() {
    mongoose.disconnect();
}