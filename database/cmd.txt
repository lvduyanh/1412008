﻿mongoexport -d QLSP -c products -o products.json
mongoexport -d QLSP -c pdifos -o pdifos.json
mongoexport -d QLSP -c pdimages -o pdimages.json
mongoexport -d QLSP -c users -o users.json
mongoexport -d QLSP -c userinfos -o userinfos.json
mongoexport -d QLSP -c orders -o orders.json

mongoimport -d QLSP2 -c products products.json
mongoimport -d QLSP2 -c pdifos pdifos.json
mongoimport -d QLSP2 -c pdimages pdimages.json
mongoimport -d QLSP2 -c users users.json
mongoimport -d QLSP2 -c userinfos userinfos.json
mongoimport -d QLSP2 -c orders orders.json