var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var User = require('../models/user.js');
var UserInfo = require('../models/userInfo');

passport.serializeUser(function (user, done) {
    done(null, user.id);
});

passport.deserializeUser(function (id, done) {
    User.findById(id, function (err, user) {
        done(err, user);
    });
});

passport.use('local.signup',  new LocalStrategy({
    usernameField: 'username',
    passwordField: 'password',
    passReqToCallback: true
}, function (req, username, password, done) {
    req.checkBody('username', 'Invalid username').notEmpty().isLength({min: 4});
    req.checkBody('password', 'Invalid password').notEmpty().isLength({min: 4});
    req.checkBody('email', 'Invalid email').notEmpty().isEmail();
    var errors = req.validationErrors();
    if (errors) {
        var messages = [];
        errors.forEach(function(error) {
           messages.push(error.msg);
        });
        return done(null, false, req.flash('error', messages));
    }
    req.sanitize('email').escape();
    //req.sanitize('username').escape();
    var un = req.body.username.toLowerCase();
    console.log(un);
    if (un.indexOf(' ') >= 0 || un.indexOf('/') >= 0 || un.indexOf('<') >= 0 || un.indexOf('>') >= 0 || un.indexOf('\\') >= 0) {
		return done(null, false, {message: 'Invalid username'});
    }
    var pw = req.body.password;
    if (pw.indexOf(' ') >= 0) {
		return done(null, false, {message: 'Invalid password'});
    }
    if (pw != req.body.repassword) {
        return done(null, false, {message: 'Password and Repassword not match!'});
    }
    User.findOne({'username': un}, function (err, user) {
        if (err) {
            console.log(err);
            return done(err);
        }
        if (user) {
            console.log(user);
            return done(null, false, {message: 'Username is already in use.'});
        }
        console.log('create account');
        var newAcc = new User();
        newAcc.username = un;
        newAcc.password = newAcc.encyptPassword(password);
        newAcc.email = req.body.email;
        var uinfo = new UserInfo();
        uinfo.username = un;
        newAcc.save(function (err, result) {
            if (err) {
                return done(err);
            }
            uinfo.save();
            transporter.sendMail({
                from: 'noreplydawck2017@gmail.com',
                to: req.body.email,
                subject: 'Hello ' + un + '!',
                template: 'mail',
                context: {
                    layout: false,
                    un: un
                }
            }, function (err, rs) {
                if (err) {
                    console.log(err);
                }
            });
            return done(null, newAcc)
        });
    });
}))

passport.use('local.signin',  new LocalStrategy({
    usernameField: 'username',
    passwordField: 'password',
    passReqToCallback: true
}, function (req, username, password, done) {
    req.checkBody('username', 'Invalid username').notEmpty();
    req.checkBody('password', 'Invalid password').notEmpty();
    var errors = req.validationErrors();
    if (errors) {
        var messages = [];
        errors.forEach(function(error) {
            messages.push(error.msg);
        });
        return done(null, false, req.flash('error', messages));
    }
    var un = username.toLowerCase();
    User.findOne({'username': un}, function (err, user) {
        if (err) {
            console.log(err);
            return done(err);
        }
        if (!user) {
            console.log(user);
            console.log(un);
            return done(null, false, {message: 'Please check your username and password again.'});
        }
        if (!user.validPassword(password)) {
            return done(null, false, {message: 'Please check your username and password again.'});
        }
        return done(null, user);
    });
}))