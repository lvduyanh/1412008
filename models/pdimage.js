var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var imageSchema = Schema({
    idpd: {type: String, require: true},
    img1: {type: String, default: ''},
    img2: {type: String, default: ''}
});

module.exports = mongoose.model('pdimage', imageSchema);