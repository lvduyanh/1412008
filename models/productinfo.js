var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var pdinfoSchema = Schema({
    idpd: {type: String, require: true},
    nsx: {type: String, default: ''},
    view: {type: Number, default: 10},
    sold: {type: Number, default: 0}
});

module.exports = mongoose.model('pdifo', pdinfoSchema);