var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ProductSchema = Schema(
    {
        name: {type: String, default: ''},
        category: {type: String, default: ''},
        price: {type: Number, default: 0},
        quantity: {type: Number, default: 0},
        info: {type: String, default: ''},
        avatar: {type: String, default: ''}
    }
);

module.exports = mongoose.model('Product', ProductSchema);