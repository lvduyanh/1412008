var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userInfoSchema = Schema({
    username: {type: String, require: true},
    name: {type: String, default: ''},
    address: {type: String, default: ''},
    type: {type: String, default: 'user'},
    status: {type: String, default: 'ok'}
});

module.exports = mongoose.model('Userinfo', userInfoSchema);