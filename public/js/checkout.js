Stripe.setPublishableKey('pk_test_Q9b856puusxDAJyB9Y40hYDx');
//var stripe = Stripe('pk_test_Q9b856puusxDAJyB9Y40hYDx');

var $form = $('#purchase-form');

$form.submit(function (event) {
    $('#pcErr').addClass('hidden');
    $form.find('button').prop('disabled', true);
    Stripe.card.createToken({
        number: $('#card-number').val(),
        cvc: $('#card-cvc').val(),
        exp_month: $('#card-expiry-month').val(),
        exp_year: $('#card-expiry-year').val(),
        name: $('#card-name').val()
    }, stripeResponseHandler);
    return false;
});

function stripeResponseHandler(status, response) {
    if (response.error) {
        $('#pcErr').text(response.error.message);
        $('#pcErr').removeClass('hidden');
        $form.find('button').prop('disabled', false);
    } else {
        var token = response.id;
        $form.append($('<input type="hidden" name="stripeToken" />').val(token));
        $form.get(0).submit();
    }
}